from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy

extensions = [
    Extension(
        "fast_conv_cython",
        ["nnframework/utils/cs231n/fast_conv_cython.pyx"],
        include_dirs=[numpy.get_include()],
    ),
]

setup(
    name="nnframework",
    version="0.1",
    description="Code for seminar about good python practices",
    url="",
    author="Anton DOROZHKO",
    author_email="anton.dorozhko@wintics.com",
    license="",
    packages=["nnframework"],
    test_suite="tests",
    zip_safe=False,
    ext_modules=cythonize(extensions),
)

from typing import Optional, Tuple
import numpy as np

from nnframework.base import Layer


class Linear(Layer):
    """
    Linear/FullyConnected layer.
    """

    def __init__(self, in_features: int, out_features: int):
        """
        Applies linear transformation to data

        .. math::
           y = xA^T + b

        https://pytorch.org/docs/stable/generated/torch.nn.Linear.html

        Args:
            in_features (int): size of each input sample.
            out_features (int): size of each output sample.

        """
        k = (1.0 / in_features) ** 0.5
        self.weight = np.random.uniform(
            low=-k, high=k, size=(out_features, in_features)
        )
        self.bias = np.random.uniform(low=-k, high=k, size=(1, out_features))
        self._dw = None
        self._db = None
        self._x = np.zeros(1)

    @property
    def weights(self) -> Optional[Tuple[np.array, np.array]]:
        return self.weight, self.bias

    @property
    def gradients(self) -> Optional[Tuple[np.array, np.array]]:
        if self._dw is None or self._db is None:
            return None
        return self._dw, self._db

    def forward(self, x: np.array) -> np.array:
        self._x = np.array(x, copy=True)
        return np.dot(x, self.weight.T) + self.bias

    def backward(self, dy: np.array) -> np.array:
        n = self._x.shape[0]
        self._dw = np.dot(dy.T, self._x) / n
        self._db = np.sum(dy, axis=0, keepdims=True) / n
        return np.dot(dy, self.weight)

    def set_weights(self, w: np.array, b: np.array) -> None:
        """Set weigths and biases

        Args:
            w (np.array): weights
            b (np.array): biases
        """
        self.weight = w
        self.bias = b

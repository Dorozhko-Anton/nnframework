import numpy as np

from nnframework.base import Layer


class DropoutLayer(Layer):
    def __init__(self, keep_prob):
        """
        :param keep_prob - probability that given unit will not be dropped out
        """
        self._keep_prob = keep_prob
        self._mask = None

    def forward(self, x: np.array) -> np.array:
        if self.train:
            self._mask = np.random.rand(*x.shape) < self._keep_prob
            return self._apply_mask(x, self._mask)
        else:
            return x

    def backward(self, dy: np.array) -> np.array:
        return self._apply_mask(dy, self._mask)

    def _apply_mask(self, array: np.array, mask: np.array) -> np.array:
        array *= mask
        array /= self._keep_prob
        return array

import numpy as np

from nnframework.base import Layer


class FlattenLayer(Layer):
    def __init__(self):
        self._shape = ()

    def forward(self, x: np.array) -> np.array:
        """
        :param x - ND tensor with shape (n, ..., channels)
        :output - 1D tensor with shape (n, 1)
        ------------------------------------------------------------------------
        n - number of examples in batch
        """
        self._shape = x.shape
        return np.ravel(x).reshape(x.shape[0], -1)

    def backward(self, dy: np.array) -> np.array:
        """
        :param dy - 1D tensor with shape (n, 1)
        :output - ND tensor with shape (n, ..., channels)
        ------------------------------------------------------------------------
        n - number of examples in batch
        """
        return dy.reshape(self._shape)

import numpy as np

from nnframework.base import Layer


class ReluLayer(Layer):
    def __init__(self):
        self._z = None

    def forward(self, x: np.array) -> np.array:
        """
        :param x - ND tensor with shape (n, ..., channels)
        :output ND tensor with shape (n, ..., channels)
        ------------------------------------------------------------------------
        n - number of examples in batch
        """
        self._z = np.maximum(0, x)
        return self._z

    def backward(self, dy: np.array) -> np.array:
        """
        :param dy - ND tensor with shape (n, ..., channels)
        :output ND tensor with shape (n, ..., channels)
        ------------------------------------------------------------------------
        n - number of examples in batch
        """
        dz = np.array(dy, copy=True)
        dz[self._z <= 0] = 0
        return dz


class SoftmaxLayer(Layer):
    def __init__(self):
        self._z = None

    def forward(self, x: np.array) -> np.array:
        """
        :param x - 2D tensor with shape (n, k)
        :output 2D tensor with shape (n, k)
        ------------------------------------------------------------------------
        n - number of examples in batch
        k - number of classes
        """
        e = np.exp(x - x.max(axis=1, keepdims=True))
        self._z = e / np.sum(e, axis=1, keepdims=True)
        return self._z

    def backward(self, dy: np.array) -> np.array:
        """
        :param dy - 2D tensor with shape (n, k)
        :output 2D tensor with shape (n, k)
        ------------------------------------------------------------------------
        n - number of examples in batch
        k - number of classes
        """
        return dy

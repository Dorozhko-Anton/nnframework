from .layers import Linear

# from .optimizers
# from .model


def joke():
    """Tell a joke.

    Returns:
        str: font joke
    """
    return (
        "Helvetica and Times New Roman walk into a bar"
        "Get out of here!” shouts the bartender. “We don’t serve your type."
    )

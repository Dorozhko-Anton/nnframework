from typing import List

from nnframework.base import Optimizer, Layer


class GradientDescent(Optimizer):
    """SGD Optimizer."""

    def __init__(self, lr: float):
        """SGD.

        Args:
            lr (float): learning rate
        """
        self._lr = lr

    def update(self, layers: List[Layer]) -> None:
        """Make one step.

        Args:
            layers (List[Layer]): list of layers to update.
        """
        for layer in layers:
            weights, gradients = layer.weights, layer.gradients
            if weights is None or gradients is None:
                continue

            (w, b), (dw, db) = weights, gradients
            layer.set_weights(w=w - self._lr * dw, b=b - self._lr * db)

from abc import ABC, abstractmethod
from typing import Optional, Tuple, List
import numpy as np


class Layer(ABC):
    """
    Base class for layers.
    """

    @property
    def weights(self) -> Optional[Tuple[np.array, np.array]]:
        """
        Returns weights tensor if layer is trainable
        Returns None for non-trainable layers
        """
        return None

    @property
    def gradients(self) -> Optional[Tuple[np.array, np.array]]:
        """
        Returns bias tensor if layer is trainable.
        Returns None for non-trainable layers.
        """
        return None

    @property
    def train(self) -> bool:
        """Run in train mode."""
        return True

    @abstractmethod
    def forward(self, x: np.array) -> np.array:
        """
        Perform layer forward propagation logic.

        Args:
            x (np.array): input tensor

        Returns:
            np.array: output tensor
        """

    @abstractmethod
    def backward(self, dy: np.array) -> np.array:
        """
        Perform backward pass.

        Args:
            dy (np.array): gradient of the output

        Returns:
            np.array: gradients for the input
        """

    def set_weights(self, w: np.array, b: np.array) -> None:
        """Set weigths and biases

        Args:
            w (np.array): weights
            b (np.array): biases
        """


class Optimizer(ABC):
    """
    Optimizers base class.
    """

    @abstractmethod
    def update(self, layers: List[Layer]) -> None:
        """
        Updates weights in trainable layers
        """

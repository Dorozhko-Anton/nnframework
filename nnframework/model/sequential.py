from typing import List, Dict  # , Callable, Optional

import time
import numpy as np

from nnframework.base import Layer, Optimizer

from nnframework.utils import generate_batches, format_time
from nnframework.utils.metrics import softmax_accuracy, softmax_cross_entropy

import tqdm


class Sequential:
    """
    Sequential container for layers.
    """

    def __init__(self, layers: List[Layer], optimizer: Optimizer):
        self._layers = layers
        self._optimizer = optimizer

        self._train_acc = []
        self._test_acc = []
        self._train_loss = []
        self._test_loss = []

    def train(
        self,
        x_train: np.array,
        y_train: np.array,
        x_test: np.array,
        y_test: np.array,
        epochs: int,
        batch: int = 64,
        verbose: bool = False,
        # callback: Optional[Callable[[Sequential], None]] = None,
    ) -> None:
        """
        Train and test a sequential model.

        n_train - number of examples in train data set
        n_test - number of examples in test data set
        k - number of classes

        Args:
            x_train (np.array): ND feature tensor with shape (n_train, ...)
            y_train (np.array): 2D one-hot labels tensor with shape (n_train, k)
            x_test (np.array): ND feature tensor with shape (n_test, ...)
            y_test (np.array): 2D one-hot labels tensor with shape (n_test, k)
            epochs (int): number of epochs used during model training
            batch (int, optional):  size of batch used during model training. Defaults to 64.
            verbose (bool, optional): Default False.
            callback (Optional[Callable[[Sequential], None]], optional):
                    function that will be executed at the end of each epoch.
                    Defaults to None.
        """
        for epoch in range(epochs):
            epoch_start = time.time()
            y_hat = np.zeros_like(y_train)

            for idx, (x_batch, y_batch) in tqdm.tqdm(
                enumerate(generate_batches(x_train, y_train, batch))
            ):
                y_hat_batch = self._forward(x_batch)
                error = y_hat_batch - y_batch
                self._backward(error)
                self._update()

                y_hat[idx * batch : idx * batch + y_hat_batch.shape[0]] = y_hat_batch

            self._train_acc.append(softmax_accuracy(y_hat, y_train))
            self._train_loss.append(softmax_cross_entropy(y_hat, y_train))

            y_hat = self._forward(x_test)
            test_acc = softmax_accuracy(y_hat, y_test)
            self._test_acc.append(test_acc)
            test_loss = softmax_cross_entropy(y_hat, y_test)
            self._test_loss.append(test_loss)

            if verbose:
                epoch_time = format_time(start_time=epoch_start, end_time=time.time())
                print(
                    "iter: {:05} | test loss: {:.5f} | test accuracy: {:.5f} | time: {}".format(
                        epoch + 1, test_loss, test_acc, epoch_time
                    )
                )

    def _forward(self, x: np.array) -> np.array:
        activation = x
        for layer in self._layers:
            activation = layer.forward(x=activation)
        return activation

    def _backward(self, x: np.array) -> None:
        activation = x
        for layer in reversed(self._layers):
            activation = layer.backward(dy=activation)

    def _update(self) -> None:
        self._optimizer.update(layers=self._layers)

    @property
    def history(self) -> Dict[str, List[float]]:
        return {
            "train_acc": self._train_acc,
            "test_acc": self._test_acc,
            "train_loss": self._train_loss,
            "test_loss": self._test_loss,
        }

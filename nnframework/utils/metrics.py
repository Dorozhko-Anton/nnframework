import numpy as np

from .core import convert_prob2one_hot


def softmax_accuracy(y_hat: np.array, y: np.array) -> float:
    """Softmax accuracy

    Args:
        y_hat (np.array): probs predicitons
        y (np.array): 2D one-hot ground truth labels tensor with shape (n, k)

    Returns:
        float: accuracy
    """

    y_hat = convert_prob2one_hot(y_hat)
    return (y_hat == y).all(axis=1).mean()


def softmax_cross_entropy(y_hat, y, eps=1e-20) -> float:
    """Softmax cross entropy

    Args:
        y_hat (np.array): probs predicitons
        y (np.array): 2D one-hot ground truth labels tensor with shape (n, k)

    Returns:
        float: cross_entropy
    """
    n = y_hat.shape[0]
    return -np.sum(y * np.log(np.clip(y_hat, eps, 1.0))) / n

.. NNFramework documentation master file, created by
   sphinx-quickstart on Wed Sep  9 10:42:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NNFramework's documentation!
=======================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

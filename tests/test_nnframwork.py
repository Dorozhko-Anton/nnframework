"""Check."""

import unittest
import nnframework


class TestNNframework(unittest.TestCase):
    """
    Test nnframework module.

    Args:
        unittest ([type]): [description]
    """

    def test_add(self):
        """Test simple add function."""
        self.assertAlmostEqual((1 + 2), 3)

    def test_add_fail(self):
        """Test fail."""
        self.assertAlmostEqual((2 + 2), 4)

    def test_nnframework_joke(self):
        """Test joke."""
        self.assertAlmostEqual(nnframework.joke(), nnframework.joke())

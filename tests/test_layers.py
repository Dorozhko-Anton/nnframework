import unittest
from typing import Callable
import numpy as np
import torch
import torch.nn as nn


import nnframework


def check_grad(func: Callable, grad: Callable, x0: np.array):
    """Check gradient wrt to x0

    Args:
        func (Callable): function
        grad (Callable): gradient wrt to x0
        x0 (np.array): point of interest

    Returns:
        float: diff between grad and gradapprox
    """
    eps = 1e-6
    x01d = x0.ravel()
    first_order_approx = np.zeros_like(x01d)

    for i, _ in enumerate(x01d):
        x0_plus = np.copy(x01d)
        x0_plus[i] += eps
        x0_plus = x0_plus.reshape(x0.shape)

        x0_minus = np.copy(x01d)
        x0_minus[i] -= eps
        x0_minus = x0_minus.reshape(x0.shape)

        first_order_approx[i] = np.sum((func(x0_plus) - func(x0_minus)) / (2 * eps))

    real_grad = grad(np.ones_like(func(x0)))
    gradapprox = first_order_approx.reshape(real_grad.shape)

    numerator = np.linalg.norm(real_grad - gradapprox)  # Step 1'
    denominator = np.linalg.norm(real_grad) + np.linalg.norm(gradapprox)
    difference = numerator / denominator
    return difference


class TestLinearLayer(unittest.TestCase):
    """Test Linear layer."""

    def test_init(self):
        """Test linear initialization."""
        in_features = 2
        out_features = 3
        linear = nnframework.layers.Linear(in_features, out_features)

        self.assertEqual(linear.weight.shape, (out_features, in_features))
        self.assertEqual(linear.bias.shape, (1, out_features))

    def test_vs_pytorch(self):
        """Test linear vs pytorch."""
        input_np = np.ones((1, 2))
        in_features = 2
        out_features = 3
        linear = nnframework.layers.Linear(in_features, out_features)

        torch_linear = nn.Linear(in_features, out_features)
        torch_linear.weight = nn.Parameter(torch.Tensor(linear.weight))
        torch_linear.bias = nn.Parameter(torch.Tensor(linear.bias.squeeze()))

        linear_result = linear.forward(input_np)
        torch_result = torch_linear.forward(torch.Tensor(input_np)).detach().numpy()

        self.assertTrue(np.allclose(linear_result, torch_result))

    def test_backprop(self):
        """Test gradient wrt to input with first order difference."""
        input_np = np.ones((1, 2))
        in_features = 2
        out_features = 3
        linear = nnframework.layers.Linear(in_features, out_features)

        diff = check_grad(func=linear.forward, grad=linear.backward, x0=input_np)
        self.assertTrue(np.isclose(diff, 0))

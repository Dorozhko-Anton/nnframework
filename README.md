# NNFramework

NNFramework with basic layers and optimizers + good practices of python development


Usage: 

```
git clone
```

Dev:

after you cloned the repo the first time install pre-commit hooks 

```
pre-commit install
```

```
conda create -n seminar python=3.8
conda activate seminar
pip install -r requirements.txt

pip install pre-commit

```